import React from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom'

import { HomePage, SignInPage, SignUpPage, ProfilePage } from './pages'
import  MainLayout from "./components/Layouts/MainLayout";
import { PublicRoute, PrivateRoute } from './routes'

import 'antd/dist/antd.css';
import './assets/scss/reset.scss'
import './assets/scss/global.scss'
import { DataProvider } from './store';
import DetailPostPage from './pages/DetailPostPage/DetailPostPage';

require('dotenv').config()
function App() {
  return (
    <DataProvider>
      <Router>
        <div className="app">
          <Switch>
            <PrivateRoute exact path='/' layout={MainLayout} >
              <HomePage />
            </PrivateRoute>
            <PrivateRoute exact path='/:username' layout={MainLayout} >
              <ProfilePage/>
            </PrivateRoute>
            <PrivateRoute exact path='/p/:postId' layout={MainLayout} >
              <DetailPostPage/>
            </PrivateRoute>
            <PublicRoute exact path='/account/login'>
              <SignInPage />
            </PublicRoute>
            <PublicRoute exact path='/account/signup'>
              <SignUpPage />
            </PublicRoute>
          </Switch>
        </div>
      </Router></DataProvider>
  );
}

export default App;
