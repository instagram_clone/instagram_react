import {useMemo, useContext} from 'react'
import {DataContext} from '../store'

function useStore(){
    const {state} = useContext(DataContext);
    return useMemo(() => state, [state])
}
export default useStore