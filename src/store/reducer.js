import {SET_AUTH, SET_USER,FETCH_POST_LIST, LIKE_POST, UNLIKE_POST, ADD_COMMENT_POST,
     FETCH_DATA_USER, CREATE_POST, CLEAR, FETCH_DETAIL_POST, FOLLOW_USER, UNFOLLOW_USER, DELETE_POST} from './constants'
import {initState} from './DataProvider'

function reducer(state = initState, action){
    switch (action.type) {
        case SET_AUTH:{
            const token = action.payload;
            const auth = {...state.auth, token}
            return {...state, auth}
        }
        case SET_USER:{
            const userInfo = action.payload
            const auth = {...state.auth, userInfo}
            return {...state, auth}
        }
        case FETCH_POST_LIST:{
            const items = action.payload;
            const loading = false;
            const posts= {...state.posts, loading, items}
            return {...state, posts}
        }
        case CREATE_POST: {
            const post = action.payload;
            const items  = [...state.posts.items];
            items.push(post)
            return {...state,posts: {...state.posts, items}}
        }
        case DELETE_POST:{
            const items  = [...state.posts.items];
            const newData = items.filter(x=> x._id !== action.payload)
            return {...state,posts: {...state.posts, items: newData}}
        }
        case FETCH_DETAIL_POST:{
            return {...state,detailPost : action.payload}
        }
        case LIKE_POST:{
            const postId = action.payload;
            const authUser = {...state.auth.userInfo}
            const items  = [...state.posts.items];
            items.map(item=>{
                if(item._id === postId){
                    item.likes.push(authUser._id)
                }
                return item
            })
            return {...state,posts: {...state.posts, items}}
        }
        case UNLIKE_POST:{
            const postId = action.payload;
            const authUser = {...state.auth.userInfo}
            const items  = [...state.posts.items]
            items.map(item=>{
                if(item._id === postId){
                    const index = item.likes.indexOf(authUser._id);
                    item.likes.splice(index, 1)
                }
                return item
            })
            return {...state,posts: {...state.posts, items}}
        }
        case ADD_COMMENT_POST: {
            const comment = action.payload;
            const items  = [...state.posts.items];
            items.map(item=>{
                if(item._id === comment.postId){
                    item.comments.push(comment)
                }
                return item
            })
            return {...state,posts: {...state.posts, items}}
        }
        case FETCH_DATA_USER:{
            const userProfile = action.payload;
            return {...state, userProfile}
        }
        case FOLLOW_USER: {
            const user = action.payload;
            const userProfile = {...state.userProfile, user} 
            return {...state,userProfile}
        }
        case UNFOLLOW_USER: {
            const user = action.payload;
            const userProfile = {...state.userProfile, user} 
            return {...state,userProfile}
        }
        case CLEAR:{
            return {...initState}
        }
        default:
            return state
    }
}

export default reducer