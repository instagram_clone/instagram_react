import * as types from './constants'

const setAuth = (token) =>{
    return {
        type: types.SET_AUTH,
        payload : token
    }
}

const setUser = (user)=>{
    return {
        type: types.SET_USER,
        payload: user
    }
}

const listPosts = (posts)=>{
    return{
        type: types.FETCH_POST_LIST,
        payload: posts
    }
}
const addPost = (data)=>{
    return{
        type: types.CREATE_POST,
        payload: data
    }
}
const deletePost = (id)=>{
    return {
        type: types.DELETE_POST,
        payload: id
    }
}

const likePost = (postId)=>{
    return{
        type: types.LIKE_POST,
        payload: postId
    }
}
const unlikePost = (postId)=>{
    return{
        type: types.UNLIKE_POST,
        payload: postId
    }
}

const addComment = (data)=>{
    return{
        type: types.ADD_COMMENT_POST,
        payload: data
    }
}

const fetchDataUser = (data)=>{
    return {
        type: types.FETCH_DATA_USER,
        payload: data
    }
}

const follow =(data)=>{
    return {
        type: types.FOLLOW_USER,
        payload: data
    }
}
const unfollow =(data)=>{
    return {
        type: types.UNFOLLOW_USER,
        payload: data
    }
}


const detailPost = (data)=>{
    return{
        type: types.FETCH_DETAIL_POST,
        payload: data
    }
}
const logout = ()=>{
    return{
        type: types.CLEAR
    }
}
export default {
    setAuth, 
    setUser, 
    listPosts,
    addPost,
    deletePost,
    likePost,
    unlikePost,
    addComment,
    fetchDataUser,
    follow,
    unfollow,
    detailPost,
    logout
}
