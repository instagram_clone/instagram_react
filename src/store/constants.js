export const SET_AUTH = "SET_AUTH"
export const SET_USER = "SET_USER"


export const FETCH_POST_LIST = "FETCH_POST_LIST"
export const FETCH_DETAIL_POST = "FETCH_DETAIL_POST"
export const CREATE_POST = "CREATE_POST"
export const DELETE_POST = "DELETE_POST"

export const LIKE_POST = "LIKE_POST"
export const UNLIKE_POST = "UNLIKE_POST"

export const FETCH_COMMENTS_POST = "FETCH_COMMENTS_POST"
export const ADD_COMMENT_POST = "ADD_COMMENT_POST"


export const FETCH_DATA_USER = "FETCH_DATA_USER"
export const FOLLOW_USER = "FOLLOW_USER"
export const UNFOLLOW_USER = "UNFOLLOW_USER"

export const CLEAR = "CLEAR"




