import React, {useReducer} from 'react'
import Cookie from 'js-cookie'

import {reducer} from '.'

const DataContext = React.createContext();
const userInfo = Cookie.getJSON('userInfo') || null
const initState = {
    auth: {
        token : null,
        userInfo,
        isAuthenticated : false,
        initLoading: true
    },
    posts: {
        loading: true,
        pagination: {
            _page: 1,
            _limit: 10
        },
        items: []
    }
}
function DataProvider ({children}){
    const [state, dispatch] = useReducer(reducer, initState);

    return (
        <DataContext.Provider value={{state, dispatch}}>
            {children}
        </DataContext.Provider>
    )
}

export {initState, DataContext, DataProvider}