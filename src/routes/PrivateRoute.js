import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useRouter } from '../hooks';
import Cookie from 'js-cookie'
//import Loading from '../components/Loading/Loading';

const PrivateRoute = ({ redirectPath = '/account/login', path, children, layout: Layout , ...rest}) => {
    const userInfo = Cookie.getJSON('userInfo')
    //const loading = false;
    const router = useRouter()
    if (userInfo) {
        return (
            <Route path={path} {...rest} >
                <Layout>
                    {children}
                </Layout>
            </Route>
        )
    }
    // if(loading){
    //     return <Loading/>
    // }
    const from = router.pathname;
    return <Redirect to={{ pathname: redirectPath, state: { from } }} />
}
export default PrivateRoute