import React from 'react'
import {Route} from 'react-router-dom'

const PublicRoute = ( {path ,exact , children, ...rest })=>{
    return (
        <Route path= {path} {...rest}>
            {children}
        </Route>
    )
}

export default PublicRoute