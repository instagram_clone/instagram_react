import React from 'react'
import Header from '../Header/Header'

function MainLayout({children}) {

    return (
        <div className="main-layout">
            <Header/>
            {children}
        </div>
    )
}

export default MainLayout