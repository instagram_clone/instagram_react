import React from 'react'

import './Loading.scss'
function Loading() {
    return (
        <div className='loading'>
            <div className='loading__img'></div>
        </div>
    )
}

export default Loading
