import React from 'react'
import Cookies from 'js-cookie'
import './Header.scss'
import { Input, Avatar,Popover, Button } from 'antd';
import { SearchOutlined, HomeOutlined} from '@ant-design/icons';
import { Link } from 'react-router-dom';
import { useStore, useRouter , useDispatch} from '../../hooks'
import { actions } from '../../store';

function Header() {
    const { auth: { userInfo } } = useStore()
    const router = useRouter();
    const dispatch = useDispatch()
    const  onLogout = ()=>{
        Cookies.remove('userInfo');
        Cookies.remove('token');

        dispatch(actions.logout())
        router.push('/account/login')
    }
    const content = (
        <Button onClick={()=>onLogout()}>Log out</Button>
      );
    return (
        <header>
            <div className="container header">
                <Link to='/' className='header__logo '>
                    <img src="https://www.instagram.com/static/images/web/mobile_nav_type_logo.png/735145cfe0a4.png" alt="logo" />
                </Link>
                <div className='header__search'>
                    <Input className='header-search' type='search' placeholder="Search" prefix={<SearchOutlined />} />
                </div>
                <ul className='header__right'>
                    <li className='header__right__item'>
                        <Link to='/'>
                            <HomeOutlined className='header__right__item' />
                        </Link>
                    </li>
                    <li className='header__right__item header__right--profile '>
                        <Popover placement="bottom"  content={content} trigger="hover">
                            <Link to={`/${userInfo.username}`}>
                                <Avatar className='header-right-item' src={userInfo.profilePictureUrl} />
                            </Link>
                        </Popover>
                    </li>

                </ul>
            </div>
        </header>
    )
}

export default Header
