import React from 'react';
import { useDispatch } from '../../hooks'
import './Interaction.scss'
import { HeartFilled, CommentOutlined, HeartOutlined } from '@ant-design/icons';
import postApi from '../../api/postApi';
import { actions } from '../../store';
import { Link } from 'react-router-dom';

function Interaction(props) {
    const { item, userInfo } = props;
    const dispatch = useDispatch();

    const likePost = async (postId) => {
        const data = await postApi.likePost(postId);
        dispatch(actions.likePost(data._id))

    }
    const unlikePost = async (postId) => {
        const data = await postApi.unlikePost(postId);
        dispatch(actions.unlikePost(data._id))
    }

    return (
        <div>
            {item && <div className='article__interaction'>
                <ul className='article__interaction__icons'>
                    <li>
                        {item.likes.includes(userInfo._id) ? <HeartFilled onClick={() => unlikePost(item._id)} style={{color: '#e94855'}} /> : <HeartOutlined onClick={() => likePost(item._id)} />}
                    </li>
                    <li>
                        <CommentOutlined onClick={props.focusInputComment} />
                    </li>
                </ul>
                <div className='article__interaction__content'>
                    <div className='likes-number name'>
                        {item.likes.length} likes
                </div>
                    <ul className="comment-list">
                        <li className='comment-item comment-item-author'>
                            <Link to={`${item.byUser.username}`} className='name'>{item.byUser.username}</Link>
                            <div className='caption'>{item.caption}</div>
                        </li>
                        {item.comments && item.comments.map((comment, index) => {
                            return (
                                <li key={index} className='comment-item'>
                                    <Link to={`${comment.byUser.username}`} className='name'>{comment.byUser.username}</Link>
                                    <div className='caption'>{comment.content}</div>
                                </li>
                            )
                        })}
                    </ul>
                </div>
            </div>}
        </div>
    )
}

export default Interaction
