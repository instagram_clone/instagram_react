import React, { useState, useRef } from 'react'
import { Link } from 'react-router-dom'
import './Article.scss'
import { Avatar } from 'antd';
import { CloseCircleOutlined } from '@ant-design/icons';
import Interaction from '../Interaction/Interaction';
import postApi from '../../api/postApi';
import { useDispatch } from '../../hooks'
import { actions } from '../../store';
function Article(props) {
    const { item, userInfo } = props;

    const inputRef = useRef()
    const dispatch = useDispatch()
    const [comment, setComment] = useState('')
    const addComment = async (id, data) => {
        const response = await postApi.addComment(id, data);
        const comment = {
            content: response.comment.content,
            postId: response.comment.postId,
            byUser: { username: userInfo.username }
        }
        dispatch(actions.addComment(comment))
        setComment('')
    }

    const focusInputComment = () => {
        inputRef.current.focus()
    }

    const onDeletePost = async (id) => {
        try {
            await postApi.deletePost(id);
            dispatch(actions.deletePost(id))
        } catch (error) {
            console.log("Failed to Detele Post: ", error)
        }

    }
    return (
        <div className='article'>
            <div className='article__header'>
                <div className='article__header__info'>
                    <Link to={`${item.byUser.username}`} ><Avatar className='avatar' src={item.byUser.profilePictureUrl} /></Link>
                    <Link to={`${item.byUser.username}`} className='name'>{item.byUser.username}</Link>
                </div>
                {item.byUser._id === userInfo._id &&
                    <div className='article__header__more'
                        onClick={() => onDeletePost(item._id)}
                    ><CloseCircleOutlined /></div>
                }

            </div>
            <div className='article__image'>
                <img src={item.imageUrl} alt='...' />
            </div>
            <Interaction item={item} userInfo={userInfo} focusInputComment={focusInputComment} />
            <div className='article__comment'>
                <input type='text'
                    ref={inputRef}
                    placeholder='Add a comment...'
                    name='comment'
                    value={comment}
                    onChange={(e) => setComment(e.target.value)} />
                <button onClick={() => addComment(item._id, { byUser: userInfo._id, content: comment })} disabled={!comment} >Post</button>
            </div>
        </div>
    )
}

export default Article
