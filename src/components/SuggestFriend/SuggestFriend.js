import React from 'react'

import './SuggestFriend.scss'
import { Avatar } from 'antd';
function SuggestFriend() {
    return (
        <div className='suggest'>
            <div className='suggest__header'>
                <h3 className='suggest__header__title'>Suggestions for you</h3>
                <p className='suggest__header__more'>See All</p>
            </div>
            <ul className='suggest__list'>
                <li className='suggest__item'>
                    <Avatar className='avatar' src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSpieEIm8fvpLUTwVQOxfEZcnTOPh5M5UNPoFFeWSpQgUPlh4-3&usqp=CAU" />
                    <div className="name">
                        <span >taam_pham</span>
                        <span className="desc">Follows you</span>
                    </div>
                    <a href="/" className="suggest__item__follow">
                        Follow
                    </a>
                </li>
                <li className='suggest__item'>
                    <Avatar className='avatar' src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSpieEIm8fvpLUTwVQOxfEZcnTOPh5M5UNPoFFeWSpQgUPlh4-3&usqp=CAU" />
                    <div className="name">
                        <span >taam_pham</span>
                        <span className="desc">Follows you</span>
                    </div>
                    <a href="/" className="suggest__item__follow">
                        Follow
                    </a>
                </li>
                <li className='suggest__item'>
                    <Avatar className='avatar' src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSpieEIm8fvpLUTwVQOxfEZcnTOPh5M5UNPoFFeWSpQgUPlh4-3&usqp=CAU" />
                    <div className="name">
                        <span >taam_pham</span>
                        <span className="desc">Follows you</span>
                    </div>
                    <a href="/" className="suggest__item__follow">
                        Follow
                    </a>
                </li>
            </ul>
        </div>
    )
}

export default SuggestFriend
