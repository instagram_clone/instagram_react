//const { default: axiosClient } = require("./axiosClient");
import {default as axiosClient } from './axiosClient'

const postApi =  {
    getAll : (params) =>{
        const url = '/posts';
        return axiosClient.get(url, {params})
    },
    getOne : (id) =>{
        const url = `/posts/${id}`;
        return axiosClient.get(url)
    },
    deletePost :(id)=>{
        const url = `/posts/${id}`;
        return axiosClient.delete(url)
    },
    likePost : (postId)=>{
        const url = `/posts/${postId}/like`;
        return axiosClient.put(url)
    },
    unlikePost : (postId)=>{
        const url = `/posts/${postId}/unlike`;
        return axiosClient.put(url)
    },
    getComments: (postId)=>{
        const url = `/comments/${postId}`;
        return axiosClient.get(url)
    },
    addComment: (postId, data)=>{
        const url = `/posts/${postId}/comment`;
        return axiosClient.post(url, data)
    },
    createPost: (data)=>{
        const url = '/posts';
        return axiosClient.post(url, data)
    }
}

export default postApi