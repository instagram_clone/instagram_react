import {default as axiosClient } from './axiosClient'

const userApi =  {
    getOne : (username) =>{
        const url = `users/${username}`;
        return axiosClient.get(url);
    },
    follow : (followId)=>{
        const url = `/users/follow`;
        return axiosClient.put(url, {followId})
    },
    unfollow : (unfollowId)=>{
        const url = `/users/unfollow`;
        return axiosClient.put(url, {unfollowId})
    }
}

export default userApi