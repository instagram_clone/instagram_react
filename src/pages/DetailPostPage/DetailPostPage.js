import React, { useState, useRef, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Avatar } from 'antd';
import './DetailPostPage.scss'
import Interaction from '../../components/Interaction/Interaction';
import { useRouter, useDispatch, useStore } from '../../hooks';
import { CloseCircleOutlined } from '@ant-design/icons';
import postApi from '../../api/postApi';
import { actions } from '../../store';
import Loading from '../../components/Loading/Loading';
function DetailPostPage() {
    const [comment, setComment] = useState('')
    const inputRef = useRef()
    const router = useRouter()
    const dispatch = useDispatch()
    const { postId } = router.params;
    const { auth: { userInfo }, detailPost, posts } = useStore();
    useEffect(() => {
        const fetchDetailPost = async () => {
            try {
                const data = await postApi.getOne(postId);
                dispatch(actions.detailPost(data))
            } catch (error) {
                console.log("Failed to fetch data: ", error)
            }
        }
        fetchDetailPost()
        return () => {
            //
        }
    }, [posts])

    const addComment = async (id, data) => {
        const response = await postApi.addComment(id, data);
        const comment = {
            content: response.comment.content,
            postId: response.comment.postId,
            byUser: { username: userInfo.username }
        }
        dispatch(actions.addComment(comment))
        setComment('')
    }
    const focusInputComment = () => {
        inputRef.current.focus()
    }

    const onDeletePost = async (id) => {
        try {
            await postApi.deletePost(id);
            dispatch(actions.deletePost(id))
            router.push(`/${userInfo.username}`)
        } catch (error) {
            console.log("Failed to Detele Post: ", error)
        }

    }
    return (
        <div className='main'>
            {!detailPost ? <Loading /> :
                <div className='container'>
                    <div className='detail__post'>
                        <div className='detail__post__img'>
                            <img src={detailPost.imageUrl} alt='...' />
                        </div>
                        <div className='detail__post__content'>
                            <div className='article__header'>
                                <div className='article__header__info'>
                                    <Link to={`/${detailPost.byUser.username}`} ><Avatar className='avatar' src={detailPost.byUser.profilePictureUrl} /></Link>
                                    <Link to={`/${detailPost.byUser.username}`} className='name'>{detailPost.byUser.username}</Link>
                                </div>
                                {detailPost.byUser._id === userInfo._id &&
                                    <div className='article__header__more'
                                        onClick={() => onDeletePost(detailPost._id)}
                                    ><CloseCircleOutlined /></div>
                                }
                            </div>
                            <Interaction item={detailPost} userInfo={userInfo} focusInputComment={focusInputComment} />
                            <div className='article__comment'>
                                <input type='text'
                                    ref={inputRef}
                                    placeholder='Add a comment...'
                                    name='comment'
                                    value={comment}
                                    onChange={(e) => setComment(e.target.value)} />
                                <button
                                    onClick={() => addComment(detailPost._id, { byUser: userInfo._id, content: comment })}
                                    disabled={!comment}
                                >Post</button>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </div>
    )
}

export default DetailPostPage
