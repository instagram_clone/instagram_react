import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import Cookie from "js-cookie"
import { message, Input, Button, Divider } from 'antd';

import { useRouter, useDispatch } from '../../hooks';
import {actions} from '../../store'
import authApi from '../../api/authApi';

function SignInPage() {
    const userInfo = Cookie.getJSON('userInfo') || null
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const router = useRouter()
    const dispatch = useDispatch()
    useEffect(() => {
        if (userInfo) {
            router.push('/')
        }
        return () => {
            //
        }
    },[])
    const onSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await authApi.login({ username, password })
            if (response) {
                const {token, user} = response;
                dispatch(actions.setUser(user))
                dispatch(actions.setAuth(token))
                Cookie.set('userInfo', JSON.stringify(user))
                Cookie.set('token', JSON.stringify(token))
                message.success('Login success.');
                router.push('/')
            }
        }
        catch (error) {
            message.error('Invalid username or password');
            setUsername('')
            setPassword('')
        }
    }
    return (
        <div className='wrap-form'>
            <div className="box">
                <a href='/' className='box__logo'>
                    <img src="https://www.instagram.com/static/images/web/mobile_nav_type_logo.png/735145cfe0a4.png"
                        className="logo-img" alt="" />
                </a>
                <Divider className='box__title'>Sign In</Divider>
                <form onSubmit={(e) => { onSubmit(e) }} >
                    <Input placeholder="Username"
                        style={{ marginBottom: '15px' }}
                        value={username}
                        name="username"
                        onChange={(e) => setUsername(e.target.value)}
                    />
                    <Input type="password" placeholder="Password"
                        style={{ marginBottom: '15px' }}
                        name="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                    <Button type="primary" htmlType="submit" className="login-form-button" block> Sign In</Button>
                </form>
                <Divider>Or</Divider>
                <p className='box__dir'>Don't have an account? <Link to='/account/signup'>Sign Up</Link></p>
            </div>
        </div>
    )
}

export default SignInPage
