import React, { useState } from 'react'

import { Link } from 'react-router-dom'
import { message, Input, Button, Divider } from 'antd';
import { useRouter } from '../../hooks';
import authApi from '../../api/authApi';
function SignUpPage() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('')
    const router = useRouter()
    const onSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await authApi.register({ username, password,email })
            if (response) {
                message.success('Register success.');
                router.push('/account/login')
            }
        }
        catch (error) {
            message.error('Invalid username or password');
            setUsername('')
            setPassword('')
            setEmail('')

        }
    }
    return (
        <div className='wrap-form'>
            <div className="box">
                <a href='/' className='box__logo'>
                    <img src="https://www.instagram.com/static/images/web/mobile_nav_type_logo.png/735145cfe0a4.png"
                        className="logo-img" alt="" />
                </a>
                <Divider className='box__title'>Sign Up</Divider>
                <form onSubmit={(e) => { onSubmit(e) }}>
                    <Input placeholder="Email" style={{ marginBottom: '15px' }} name="email" value={email} onChange={(e) => setEmail(e.target.value)} />
                    <Input placeholder="Username" style={{ marginBottom: '15px' }} name="username" value={username} onChange={(e) => setUsername(e.target.value)}/>
                    <Input type="password" placeholder="Password" name="password" style={{ marginBottom: '15px' }} value={password} onChange={(e) => setPassword(e.target.value)} />
                    <Button type="primary" htmlType="submit" className="login-form-button" block> Sign up</Button>
                </form>
                <Divider>Or</Divider>
                <p className='box__dir'>Have an account? <Link to='/account/login'>Log in</Link></p>
            </div>
        </div>
    )
}

export default SignUpPage
