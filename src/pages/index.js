export {default as HomePage} from './HomePage/HomePage'
export {default as SignInPage} from './SignInPage/SignInPage'
export {default as SignUpPage} from './SignUpPage/SignUpPage'
export {default as ProfilePage} from './ProfilePage/ProfilePage'