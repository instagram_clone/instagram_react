import React, {useEffect} from 'react'

import {useStore, useDispatch} from '../../hooks'
import Article from '../../components/Article/Article'
import SuggestFriend from '../../components/SuggestFriend/SuggestFriend'

import './HomePage.scss'
import postApi from '../../api/postApi'
import { actions } from '../../store'
import Loading from '../../components/Loading/Loading'
function HomePage() {
    const {posts: {loading, pagination, items}, auth: {userInfo}} = useStore();
    const dispatch = useDispatch()
    const {  _page, _limit } = pagination;
    useEffect(() => {
        const fetchPostList = async ()=>{
            try {
                const response = await postApi.getAll({_page, _limit});
                dispatch(actions.listPosts(response))
            } catch (error) {
                console.log("Failed to fetch post list: " , error)
            }
        }
        fetchPostList()
        return () => {
            //
        }
    }, [])
    
    if(loading){
        return <Loading/>
    }
    return (
        <div>
            <div className='main'>
                <div className='container wrapper'>
                    <div>
                        {items.map((item,index)=> <Article key={index} item={item} userInfo={userInfo}/>)}
                    </div>
                    <div className='suggests'>
                        <SuggestFriend />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HomePage
