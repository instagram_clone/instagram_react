import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { useRouter, useDispatch, useStore } from '../../hooks'
import userApi from '../../api/userApi';
import { actions } from '../../store';
import Loading from '../../components/Loading/Loading';

import './ProfilePage.scss'
import { Modal, Input, Button } from 'antd';
import { HeartFilled, MessageFilled, PlusOutlined } from '@ant-design/icons';
import postApi from '../../api/postApi';
import { Link } from 'react-router-dom';
const { TextArea } = Input;

function ProfilePage() {
    const router = useRouter();
    const dispatch = useDispatch();
    const { userProfile, auth: { userInfo } } = useStore();
    const { username } = router.params;

    useEffect(() => {
        const fetchDataUser = async () => {
            try {
                const data = await userApi.getOne(username);
                dispatch(actions.fetchDataUser(data))
            } catch (error) {
                console.log("Failed to fetch data user: ", error)
            }
        }
        fetchDataUser()
        return () => {
            //
        }
    }, [username])

    const [modal, setModal] = useState(false)
    const [caption, setCaption] = useState('')
    const [image, setImage] = useState('');
    const showModal = () => {
        setModal(true)
    };

    const handleOk = (e) => {
        let formData = new FormData();
        formData.append("file", image);
        formData.append("upload_preset", "instagram-clone");
        formData.append("api_key", process.env.API_KEY_CLOUDINARY);
        axios.post('https://api.cloudinary.com/v1_1/tampham97/image/upload', formData, {
            headers: {
                "X-Requested-With": "XMLHttpRequest",
                'Content-Type': 'multipart/form-data'
            }
        })
            .then((result) => {
                const imageUrl = result.data.url;
                const createPost = async () => {
                    const response = await postApi.createPost({ caption, imageUrl })
                    dispatch(actions.addPost(response.post))
                    router.push('/')
                }
                createPost()
            })
            .catch((err) => {
                console.log(err);
            })
        setModal(false)
    };

    const handleCancel = (e) => {
        setModal(false)
    };

    const followUser = async (followId) => {
        try {
            const response = await userApi.follow(followId)
            dispatch(actions.follow(response))
        } catch (error) {
            console.log("Failed to request: ", error);
        }
    }
    const unfollowUser = async (followId) => {
        try {
            const response = await userApi.unfollow(followId)
            dispatch(actions.unfollow(response))
        } catch (error) {
            console.log("Failed to request: ", error);
        }
    }
    return (
        <div>
            {!userProfile ? <Loading /> :
                <div className='main'>
                    <div className='container'>
                        <div className='profile__header'>
                            <div className='profile__header__avatar'>
                                <img src={userProfile.user.profilePictureUrl} alt='...' />
                            </div>
                            <div className='profile__header__info'>
                                <div className='profile__header__name'>
                                    {userProfile.user.username}
                                    {(userInfo.username !== userProfile.user.username) &&
                                        (
                                            !(userProfile.user.followers.find(x=>x === userInfo._id)) ?
                                                <Button type="primary" onClick={() => followUser(userProfile.user._id)}>Follow</Button> :
                                                <Button onClick={() => unfollowUser(userProfile.user._id)}>Unfollow</Button>
                                        )
                                    }
                                </div>
                                <ul className='profile__header__follow'>
                                    <li><span>{userProfile.posts.length}</span> posts</li>
                                    <li><span>{userProfile.user.followers.length} </span> followers</li>
                                    <li><span>{userProfile.user.following.length}</span> following</li>
                                </ul>


                            </div>
                        </div>
                        <div className='profile__content'>
                            {userProfile.posts.map((post, index) =>
                                <Link to={`/p/${post._id}`} key={index}>
                                    <div className='profile__content__post'
                                        style={{ backgroundImage: `url(${post.imageUrl})` }}
                                    >
                                        <div className='profile__content__post__wrap' >
                                            <div className='profile__content__post__info'>
                                                <span><HeartFilled style={{ color: '#ffffff' }} /> {post.likes.length} </span>
                                                <span><MessageFilled style={{ color: '#ffffff' }} />  {post.comments.length}</span>
                                            </div>
                                        </div>
                                    </div>
                                </Link>
                            )}
                        </div>
                    </div>
                    <div className='btn-add' onClick={() => showModal()}><PlusOutlined className='add-icon' /></div>
                    <Modal
                        title="Create a new Post"
                        visible={modal}
                        onOk={() => handleOk()}
                        onCancel={() => handleCancel()}
                        okText="Create"
                    >
                        <TextArea rows={4} style={{ marginBottom: "2rem" }}
                            name='caption'
                            value={caption}
                            onChange={(e) => setCaption(e.target.value)}
                        />

                        <input className='btn-upload' type='file' name='image' onChange={(e) => setImage(e.target.files[0])} />
                    </Modal>
                </div>}
        </div>
    )
}

export default ProfilePage
