# Instagram-clone (FE)
Clone Instagram website using MERN stack

## Demo
[Link](https://instagram123-react.web.app/)

## Requirements
- Install Node

## Using
- react & hooks
- react-router-dom
- ant-design UI
- store photos by Cloudinary

## Features
- Add, Remove Post
- Like, Comment Post
- Login, Register User
- Follow User

## Installation
Use the package manager npm to install dependencies.
> npm i

## Run
> npm start


## BackEnd
[Link](https://gitlab.com/instagram_clone/instagram_api)




